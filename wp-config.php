<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'digitalstory');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'QqX+j+??*:-++y%2D)Z@0+W%jgppLY2&KuAYg$s5-.(i QUyrI$)4zz#xX#Q1yXq');
define('SECURE_AUTH_KEY',  'g&4P1O;8n-!aJm3es70e9p[Jk:k4UXAV`0k@]l6:U )Xo%UL+WE=1d08]~h4(5VM');
define('LOGGED_IN_KEY',    'dDK5|@o^To D>b&moC$]CD*)L,LEfuFW|-E[zx|bp!z9`:A!Cus9*Bev/VQRR4Xu');
define('NONCE_KEY',        '+7P-cOVC~f-@rIhDq##0* 6_I;/-E=+[q0Umlq_{C[PVJ@S,= Ty=L^00_plWvb0');
define('AUTH_SALT',        'o8PZF7/MdUT|jjmQ<j-KKXo(=Eb!.UFM!1Sh3ism!FkCG_|Lx}g3otME!IoT?=&+');
define('SECURE_AUTH_SALT', '# T?UgES-^P;HQ{rBj?|=QcsF2Y+b+^[Ezo[raCF:p|?oc_hT,KZpA:;d((l!?e!');
define('LOGGED_IN_SALT',   'br9++K#b, _3OK;RE=MH^d}N[`Mv3O[3%7?ba},Dj#G^}!Z*8%IeU3}cG0BJ(R4R');
define('NONCE_SALT',       '0|AYw)_|Qu~k2D&oz7CK0<Cwb;{I&.dr688*IqL{#pODr+BWg8jQPj*`d2pfY>ZM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
